(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/dict-select.md?vue&type=template&id=fa1074da

var _hoisted_1 = {
  class: "content element-doc"
};

var _hoisted_2 = /*#__PURE__*/Object(vue_esm_browser["createStaticVNode"])("<h2 id=\"dictselect-zi-duan-xuan-ze-qi\"><a class=\"header-anchor\" href=\"#dictselect-zi-duan-xuan-ze-qi\">¶</a> DictSelect 字段选择器</h2><p>提供一个字典下拉框选择器的 <em>参考范式</em>.</p><p>实际开发中, 难免会遇到很多前后端都需要使用的枚举类型, 在前端是以下拉框的形式存在, 在后端可能是枚举类或配置表的形式.</p><p>在前后端交互中, 各自都需要维护同样的字典选项, 有点小烦人. 为简化和统一配置. 可统一由后端返回选择集. 选择集来源可以是枚举类(如<code>java</code> 的 <code>enum</code>), 也可以是数据库字典表配置.</p><p>基于 <code>ant-design-vue</code> 的 <code>select</code>.</p><p>本组件提供三种参考方式获取选择集.</p><ol><li>枚举类全类名(<code>enumClass</code>), 如 <code>com.example.StatusEnum</code>.</li><li>字典表(<code>dictTable</code>), 某一类字段项比较多时, 可单独一张表放置.</li><li>字典编号(<code>dictCode</code>), 一个编号下关联了该编号下所有枚举项. 适合枚举项较少且可能变化的字典类型.</li></ol><h3 id=\"jian-dan-shi-yong\"><a class=\"header-anchor\" href=\"#jian-dan-shi-yong\">¶</a> 简单使用</h3><p>前端指定选项集.</p>", 9);

var _hoisted_11 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("code", {
  class: "html"
}, "<template>\n  <y-dict-select placeholder=\"请选择\" v-model:value=\"myValue\" :options=\"options\" @change=\"changeEvent\" style=\"width: 300px\"></y-dict-select>\n</template>\n<script>\n  export default {\n    data() {\n        return {\n          options: [\n            {value: '1', text: '一'},\n            {value: '2', text: '二'},\n            {value: '3', text: '三'},\n          ]\n        }\n    },\n  }\n</script>\n")], -1);

var _hoisted_12 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("h3", {
  id: "hou-duan-jia-zai-xuan-xiang-ji"
}, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("a", {
  class: "header-anchor",
  href: "#hou-duan-jia-zai-xuan-xiang-ji"
}, "¶"), /*#__PURE__*/Object(vue_esm_browser["createTextVNode"])(" 后端加载选项集")], -1);

var _hoisted_13 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("p", null, "可在组件创建时掉用后端api异步加载选项集.", -1);

var _hoisted_14 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("code", {
  class: "html"
}, "<template>\n  <y-dict-select placeholder=\"请选择\" :load-options=\"loadOptions\" style=\"width: 300px\"></y-dict-select>\n</template>\n<script>\n  export default {\n    methods: {\n      // 异步加载和返回选项集\n      loadOptions() {\n        return [\n          {value: '1', text: '一'},\n          {value: '2', text: '二'},\n          {value: '3', text: '三'},\n        ];\n      }\n    }\n  }\n</script>\n")], -1);

var _hoisted_15 = /*#__PURE__*/Object(vue_esm_browser["createStaticVNode"])("<h3 id=\"api\"><a class=\"header-anchor\" href=\"#api\">¶</a> API</h3><h4 id=\"attributes\"><a class=\"header-anchor\" href=\"#attributes\">¶</a> Attributes</h4><p><code>ant-design-vue</code> <code>select</code> 的属性均可用。</p><table><thead><tr><th>参数</th><th>说明</th><th>类型</th><th>可选值</th><th>默认值</th></tr></thead><tbody><tr><td>loadOptions</td><td>加载选项集的方法</td><td>function</td><td>—</td><td>—</td></tr><tr><td>options</td><td>选项集合, <code>{value, text, extra}</code>, 优先级高于<code>loadOptions</code></td><td>array</td><td>—</td><td>—</td></tr><tr><td>excludes</td><td>需要排除的 value , 远程加载options时有效</td><td>string(逗号&#39;,&#39;分割) / array</td><td>—</td><td>—</td></tr></tbody></table>", 4);

function dict_selectvue_type_template_id_fa1074da_render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_element_demo0 = Object(vue_esm_browser["resolveComponent"])("element-demo0");

  var _component_demo_block = Object(vue_esm_browser["resolveComponent"])("demo-block");

  var _component_element_demo1 = Object(vue_esm_browser["resolveComponent"])("element-demo1");

  return Object(vue_esm_browser["openBlock"])(), Object(vue_esm_browser["createBlock"])("section", _hoisted_1, [_hoisted_2, Object(vue_esm_browser["createVNode"])(_component_demo_block, null, {
    source: Object(vue_esm_browser["withCtx"])(function () {
      return [Object(vue_esm_browser["createVNode"])(_component_element_demo0)];
    }),
    highlight: Object(vue_esm_browser["withCtx"])(function () {
      return [_hoisted_11];
    }),
    _: 1
  }), _hoisted_12, _hoisted_13, Object(vue_esm_browser["createVNode"])(_component_demo_block, null, {
    source: Object(vue_esm_browser["withCtx"])(function () {
      return [Object(vue_esm_browser["createVNode"])(_component_element_demo1)];
    }),
    highlight: Object(vue_esm_browser["withCtx"])(function () {
      return [_hoisted_14];
    }),
    _: 1
  }), _hoisted_15]);
}
// CONCATENATED MODULE: ./website/docs/zh-CN/dict-select.md?vue&type=template&id=fa1074da

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/extends.js
var helpers_extends = __webpack_require__(45);
var extends_default = /*#__PURE__*/__webpack_require__.n(helpers_extends);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/dict-select.md?vue&type=script&lang=ts


/* harmony default export */ var dict_selectvue_type_script_lang_ts = ({
  name: 'component-doc',
  components: {
    "element-demo0": function () {
      var _resolveComponent = vue_esm_browser["resolveComponent"],
          _createVNode = vue_esm_browser["createVNode"],
          _openBlock = vue_esm_browser["openBlock"],
          _createBlock = vue_esm_browser["createBlock"];

      function render(_ctx, _cache) {
        var _component_y_dict_select = _resolveComponent("y-dict-select");

        return _openBlock(), _createBlock("div", null, [_createVNode(_component_y_dict_select, {
          placeholder: "请选择",
          value: _ctx.myValue,
          "onUpdate:value": _cache[1] || (_cache[1] = function ($event) {
            return _ctx.myValue = $event;
          }),
          options: _ctx.options,
          onChange: _ctx.changeEvent,
          style: {
            "width": "300px"
          }
        }, null, 8, ["value", "options", "onChange"])]);
      }

      var democomponentExport = {
        data: function data() {
          return {
            options: [{
              value: '1',
              text: '一'
            }, {
              value: '2',
              text: '二'
            }, {
              value: '3',
              text: '三'
            }]
          };
        }
      };
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "element-demo1": function () {
      var _resolveComponent = vue_esm_browser["resolveComponent"],
          _createVNode = vue_esm_browser["createVNode"],
          _openBlock = vue_esm_browser["openBlock"],
          _createBlock = vue_esm_browser["createBlock"];

      function render(_ctx, _cache) {
        var _component_y_dict_select = _resolveComponent("y-dict-select");

        return _openBlock(), _createBlock("div", null, [_createVNode(_component_y_dict_select, {
          placeholder: "请选择",
          "load-options": _ctx.loadOptions,
          style: {
            "width": "300px"
          }
        }, null, 8, ["load-options"])]);
      }

      var democomponentExport = {
        methods: {
          // 异步加载和返回选项集
          loadOptions: function loadOptions() {
            return [{
              value: '1',
              text: '一'
            }, {
              value: '2',
              text: '二'
            }, {
              value: '3',
              text: '三'
            }];
          }
        }
      };
      return extends_default()({
        render: render
      }, democomponentExport);
    }()
  }
});
// CONCATENATED MODULE: ./website/docs/zh-CN/dict-select.md?vue&type=script&lang=ts
 
// CONCATENATED MODULE: ./website/docs/zh-CN/dict-select.md



dict_selectvue_type_script_lang_ts.render = dict_selectvue_type_template_id_fa1074da_render

/* harmony default export */ var dict_select = __webpack_exports__["default"] = (dict_selectvue_type_script_lang_ts);

/***/ })

}]);
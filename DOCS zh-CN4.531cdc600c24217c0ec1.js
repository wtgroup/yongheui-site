(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ 696:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/nest-criterion.md?vue&type=template&id=7ef0e5d4

var nest_criterionvue_type_template_id_7ef0e5d4_hoisted_1 = {
  class: "content element-doc"
};

var nest_criterionvue_type_template_id_7ef0e5d4_hoisted_2 = /*#__PURE__*/Object(vue_esm_browser["createStaticVNode"])("<h2 id=\"ynestcriterion-qian-tao-tiao-jian-she-ji-qi\"><a class=\"header-anchor\" href=\"#ynestcriterion-qian-tao-tiao-jian-she-ji-qi\">¶</a> YNestCriterion 嵌套条件设计器</h2><p>提供友好便捷的SQL条件可视化设置.</p><ol><li>降低使用门槛，不懂sql的业务人员可以随心所欲的关注业务逻辑，轻松设置用户人群包圈人条件.</li><li>提高便捷性，对于精通sql的技术人员，这仍然是个很好用的组件，用于界面sql.</li></ol><p><strong>使用场景</strong></p><ol><li>用户人群包圈定：后端实现准备好用户画像大宽表. 页面中使用此组件手动配置sql条件（圈人规则），发送后端解析成可执行sql，执行查询，圈定人群包.</li><li>其他需要界面设置sql的场景：本组件可以很好的助力界面配置<code>where</code>和<code>having</code>部分.</li></ol><p><strong>概念</strong></p><ul><li><code>条件</code>：一个条件表达式就是一个<code>条件</code>. 如：<code>is_student = &#39;Y&#39;</code>.</li><li><code>条件组</code>：<code>and</code> 或 <code>or</code> 连接的同层级的多个条件构成<code>条件组</code> .</li><li><code>操作符</code>(<em>operator</em>): <code>&gt;</code>, <code>=</code>, <code>&lt;</code> ...</li><li><code>字段类型</code>(<em>FieldType</em>): 对应数据库表字段的实际类型. 如 <code>String</code>,<code>Number</code>.</li><li><code>值类型</code>(<em>ValType</em>): 字段值类型由具体业务含义决定, 和实际类型无关. 如<code>是否学生</code>, 实际中一般用 <em>1:是, 0:否</em> 表达. 那么, 其<code>FieldType</code>是<code>Number</code>, 但<code>ValType</code>是<code>布尔</code>.</li></ul><p>每个条件组默认会有一个条件, 没有条件的条件组自动 <code>删除</code> , 不允许没有条件的 条件组存在. 组标题行后的 <code>新增</code> 条件组 新增下一级组. <code>新增</code> 都是新增 兄弟. 条件后新增增加兄弟条件; 组后新增新增兄弟组.</p><p><strong>使用指导</strong></p><p>实践中, 需要关注一个重要的参数: <code>criterionOptions</code>. 此参数主要和需要查询的大宽表元数据信息对应(列名, 列类型等). 这些信息需要对应的配置表描述.</p><p>元信息字段:</p><table><thead><tr><th>字段名</th><th>含义</th><th>必填?</th><th>备注</th></tr></thead><tbody><tr><td>id</td><td>字段名</td><td>Y</td><td></td></tr><tr><td>title</td><td>字段友好名称</td><td>N</td><td></td></tr><tr><td>fieldType</td><td>字段类型</td><td>Y</td><td></td></tr><tr><td>fieldType_dictText</td><td>字段类型解释</td><td>N</td><td></td></tr><tr><td>valType</td><td>字段值类型</td><td>Y</td><td>实际业务决定. 连续值; 布尔值; 离散值(枚举值). 如, &#39;是否学生&#39; 认为是&#39;布尔&#39;类型, &#39;订单状态&#39; 认为是 &#39;离散&#39; 类型. 值类型会影响可选的条件表达式<code>操作符</code>范围</td></tr><tr><td>valType_dictText</td><td>字段值类型解释</td><td>N</td><td></td></tr><tr><td>remark</td><td>备注</td><td>N</td><td>额外说明</td></tr><tr><td>options</td><td>布尔/离散 值类型 时, 会有提供下拉选择的选项集.</td><td>N</td><td></td></tr></tbody></table><h3 id=\"ji-ben-shi-yong\"><a class=\"header-anchor\" href=\"#ji-ben-shi-yong\">¶</a> 基本使用</h3><p><em>Tip: 修改后, 下方实时预览SQL片段(条件语句)</em></p>", 14);

var _hoisted_16 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("code", {
  class: "html"
}, "<template>\n  <div>\n    <y-nest-criterion ref=\"nc\" :criterionOptions=\"criterionOptions\" @change=\"changeEvent\"></y-nest-criterion>\n    <br/>\n    <div style=\"color: #722ed1; font-weight: bold; font-family: Consolas; font-size: 16px; text-align: center;\">{{sql}}</div>\n  </div>\n</template>\n\n<script>\n\nconst {RuleParser} = require('@yongheui/nest-criterion/src/tool');\n\nexport default {\n  data() {\n    return {\n      sql: '',\n      // 实践中, 此选项由后端配置表查询封装返回\n      criterionOptions: [{\n        \"id\": \"is_student\",\n        \"title\": \"是否学生\",\n        \"fieldType\": 2,\n        \"fieldType_dictText\": \"String\",\n        \"valType\": 2,\n        \"valType_dictText\": \"Bool\",\n        \"remark\": \"1: 是; 0: 否\",\n        \"options\": [{\n          \"value\": \"1\",\n          \"text\": \"是\",\n          \"extra\": null,\n          \"disabled\": null\n        }, {\n          \"value\": \"0\",\n          \"text\": \"否\",\n          \"extra\": null,\n          \"disabled\": null\n        }]\n      }, {\n        \"id\": \"age\",\n        \"title\": \"年龄段\",\n        \"fieldType\": 2,\n        \"fieldType_dictText\": \"String\",\n        \"valType\": 3,\n        \"valType_dictText\": \"Scatter\",\n        \"remark\": \"0: <=18; 1: (18,30]; 2: (30, 40]; 3: >40\",\n        \"options\": [{\n          \"value\": \"0\",\n          \"text\": \"<=18\",\n          \"extra\": \"\"\n        }, {\n          \"value\": \"1\",\n          \"text\": \"(18,30]\",\n          \"extra\": \"\"\n        }, {\n          \"value\": \"2\",\n          \"text\": \"(30, 40]\",\n          \"extra\": \"\"\n        }, {\n          \"value\": \"3\",\n          \"text\": \">40\",\n          \"extra\": \"\"\n        }]\n      }, {\n        \"id\": \"cate_id_list\",\n        \"title\": \"类目id\",\n        \"fieldType\": 62,\n        \"fieldType_dictText\": \"ArrayString\",\n        \"valType\": 1,\n        \"valType_dictText\": \"Series\",\n        \"remark\": \"\",\n        \"options\": null\n      }, {\n        \"id\": \"avg_gmv_180\",\n        \"title\": \"近半年平均gmv\",\n        \"fieldType\": 1,\n        \"fieldType_dictText\": \"Number\",\n        \"valType\": 1,\n        \"valType_dictText\": \"Series\",\n        \"remark\": \"\",\n        \"options\": null\n      }, {\n        \"id\": \"uid_consumption_level\",\n        \"title\": \"uid_消费等级\",\n        \"fieldType\": 2,\n        \"fieldType_dictText\": \"String\",\n        \"valType\": 3,\n        \"valType_dictText\": \"Scatter\",\n        \"remark\": \"区分低, 中低, 中高, 高四档(<Q1,[Q1,Q2),[Q2,Q3),Q3<=), 用户维度.\",\n        \"options\": [{\n          \"value\": \"H\",\n          \"text\": \"高\",\n          \"extra\": \"\",\n          \"disabled\": null\n        }, {\n          \"value\": \"M+\",\n          \"text\": \"中高\",\n          \"extra\": \"\",\n          \"disabled\": null\n        }, {\n          \"value\": \"M-\",\n          \"text\": \"中低\",\n          \"extra\": \"\",\n          \"disabled\": null\n        }, {\n          \"value\": \"L\",\n          \"text\": \"低\",\n          \"extra\": \"\",\n          \"disabled\": null\n        }]\n      }]\n    }\n  },\n  methods: {\n    changeEvent(dataSource) {\n      console.log('changeEvent 回调入参 dataSource :', dataSource);\n      this.sql = RuleParser.parseRule(dataSource);\n      console.log('changeEvent parsed sql ==> ', this.sql);\n    },\n  }\n\n\n}\n</script>\n")], -1);

var _hoisted_17 = /*#__PURE__*/Object(vue_esm_browser["createStaticVNode"])("<h3 id=\"api\"><a class=\"header-anchor\" href=\"#api\">¶</a> API</h3><h4 id=\"attributes\"><a class=\"header-anchor\" href=\"#attributes\">¶</a> Attributes</h4><table><thead><tr><th>参数</th><th>说明</th><th>类型</th><th>可选值</th><th>默认值</th></tr></thead><tbody><tr><td>width</td><td>宽度</td><td>number</td><td></td><td>600</td></tr><tr><td>maxDepth</td><td>组的最大深度. level 从0起始, 所以默认最大嵌套4层</td><td>number</td><td>string</td><td></td></tr><tr><td>dataSource</td><td>初始的条件值. 类似 input</td><td>select 组件的 value .</td><td>object</td><td></td></tr><tr><td>criterionOptions</td><td>条件选项集, 待从中选择, 最终生成 dataSource .</td><td>array</td><td></td><td></td></tr></tbody></table><h4 id=\"events\"><a class=\"header-anchor\" href=\"#events\">¶</a> Events</h4><table><thead><tr><th>事件名称</th><th>说明</th><th>回调参数</th></tr></thead><tbody><tr><td>change</td><td><code>id</code>, <code>operator</code>, <code>value</code> 改变时; 条件(组)增删时</td><td>dataSource</td></tr></tbody></table>", 5);

function nest_criterionvue_type_template_id_7ef0e5d4_render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_element_demo0 = Object(vue_esm_browser["resolveComponent"])("element-demo0");

  var _component_demo_block = Object(vue_esm_browser["resolveComponent"])("demo-block");

  return Object(vue_esm_browser["openBlock"])(), Object(vue_esm_browser["createBlock"])("section", nest_criterionvue_type_template_id_7ef0e5d4_hoisted_1, [nest_criterionvue_type_template_id_7ef0e5d4_hoisted_2, Object(vue_esm_browser["createVNode"])(_component_demo_block, null, {
    source: Object(vue_esm_browser["withCtx"])(function () {
      return [Object(vue_esm_browser["createVNode"])(_component_element_demo0)];
    }),
    highlight: Object(vue_esm_browser["withCtx"])(function () {
      return [_hoisted_16];
    }),
    _: 1
  }), _hoisted_17]);
}
// CONCATENATED MODULE: ./website/docs/zh-CN/nest-criterion.md?vue&type=template&id=7ef0e5d4

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/extends.js
var helpers_extends = __webpack_require__(45);
var extends_default = /*#__PURE__*/__webpack_require__.n(helpers_extends);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/nest-criterion.md?vue&type=script&lang=ts


/* harmony default export */ var nest_criterionvue_type_script_lang_ts = ({
  name: 'component-doc',
  components: {
    "element-demo0": function () {
      var _resolveComponent = vue_esm_browser["resolveComponent"],
          _createVNode = vue_esm_browser["createVNode"],
          _toDisplayString = vue_esm_browser["toDisplayString"],
          _openBlock = vue_esm_browser["openBlock"],
          _createBlock = vue_esm_browser["createBlock"];

      var _hoisted_1 = /*#__PURE__*/_createVNode("br", null, null, -1);

      var _hoisted_2 = {
        style: {
          "color": "#722ed1",
          "font-weight": "bold",
          "font-family": "Consolas",
          "font-size": "16px",
          "text-align": "center"
        }
      };

      function render(_ctx, _cache) {
        var _component_y_nest_criterion = _resolveComponent("y-nest-criterion");

        return _openBlock(), _createBlock("div", null, [_createVNode("div", null, [_createVNode(_component_y_nest_criterion, {
          ref: "nc",
          criterionOptions: _ctx.criterionOptions,
          onChange: _ctx.changeEvent
        }, null, 8, ["criterionOptions", "onChange"]), _hoisted_1, _createVNode("div", _hoisted_2, _toDisplayString(_ctx.sql), 1)])]);
      }

      var _require = __webpack_require__(44),
          RuleParser = _require.RuleParser;

      var democomponentExport = {
        data: function data() {
          return {
            sql: '',
            // 实践中, 此选项由后端配置表查询封装返回
            criterionOptions: [{
              "id": "is_student",
              "title": "是否学生",
              "fieldType": 2,
              "fieldType_dictText": "String",
              "valType": 2,
              "valType_dictText": "Bool",
              "remark": "1: 是; 0: 否",
              "options": [{
                "value": "1",
                "text": "是",
                "extra": null,
                "disabled": null
              }, {
                "value": "0",
                "text": "否",
                "extra": null,
                "disabled": null
              }]
            }, {
              "id": "age",
              "title": "年龄段",
              "fieldType": 2,
              "fieldType_dictText": "String",
              "valType": 3,
              "valType_dictText": "Scatter",
              "remark": "0: <=18; 1: (18,30]; 2: (30, 40]; 3: >40",
              "options": [{
                "value": "0",
                "text": "<=18",
                "extra": ""
              }, {
                "value": "1",
                "text": "(18,30]",
                "extra": ""
              }, {
                "value": "2",
                "text": "(30, 40]",
                "extra": ""
              }, {
                "value": "3",
                "text": ">40",
                "extra": ""
              }]
            }, {
              "id": "cate_id_list",
              "title": "类目id",
              "fieldType": 62,
              "fieldType_dictText": "ArrayString",
              "valType": 1,
              "valType_dictText": "Series",
              "remark": "",
              "options": null
            }, {
              "id": "avg_gmv_180",
              "title": "近半年平均gmv",
              "fieldType": 1,
              "fieldType_dictText": "Number",
              "valType": 1,
              "valType_dictText": "Series",
              "remark": "",
              "options": null
            }, {
              "id": "uid_consumption_level",
              "title": "uid_消费等级",
              "fieldType": 2,
              "fieldType_dictText": "String",
              "valType": 3,
              "valType_dictText": "Scatter",
              "remark": "区分低, 中低, 中高, 高四档(<Q1,[Q1,Q2),[Q2,Q3),Q3<=), 用户维度.",
              "options": [{
                "value": "H",
                "text": "高",
                "extra": "",
                "disabled": null
              }, {
                "value": "M+",
                "text": "中高",
                "extra": "",
                "disabled": null
              }, {
                "value": "M-",
                "text": "中低",
                "extra": "",
                "disabled": null
              }, {
                "value": "L",
                "text": "低",
                "extra": "",
                "disabled": null
              }]
            }]
          };
        },
        methods: {
          changeEvent: function changeEvent(dataSource) {
            console.log('changeEvent 回调入参 dataSource :', dataSource);
            this.sql = RuleParser.parseRule(dataSource);
            console.log('changeEvent parsed sql ==> ', this.sql);
          }
        }
      };
      return extends_default()({
        render: render
      }, democomponentExport);
    }()
  }
});
// CONCATENATED MODULE: ./website/docs/zh-CN/nest-criterion.md?vue&type=script&lang=ts
 
// CONCATENATED MODULE: ./website/docs/zh-CN/nest-criterion.md



nest_criterionvue_type_script_lang_ts.render = nest_criterionvue_type_template_id_7ef0e5d4_render

/* harmony default export */ var nest_criterion = __webpack_exports__["default"] = (nest_criterionvue_type_script_lang_ts);

/***/ })

}]);
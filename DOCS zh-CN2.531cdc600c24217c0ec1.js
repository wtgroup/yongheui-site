(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ 698:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/installation.md?vue&type=template&id=4559f461

var _hoisted_1 = {
  class: "content element-doc"
};

var _hoisted_2 = /*#__PURE__*/Object(vue_esm_browser["createStaticVNode"])("<h2 id=\"an-zhuang\"><a class=\"header-anchor\" href=\"#an-zhuang\">¶</a> 安装</h2><h3 id=\"npm-yarn-an-zhuang\"><a class=\"header-anchor\" href=\"#npm-yarn-an-zhuang\">¶</a> npm | yarn 安装</h3><p>推荐使用 npm 的方式安装，它能更好地和 <a href=\"https://webpack.js.org/\">webpack</a> 打包工具配合使用。</p><pre><code class=\"hljs language-shell\">npm install yongheui --save\n// 或\n// yarn add yongheui\n</code></pre><p><a href=\"https://github.com/wtgroup/yongheui\"><img src=\"https://img.shields.io/github/stars/wtgroup/yongheui.svg?style=social\" alt=\"\"></a><a href=\"https://www.npmjs.com/package/yongheui\"><img src=\"https://img.shields.io/npm/v/yongheui\" alt=\"npm\"></a></p>", 5);

function render(_ctx, _cache) {
  return Object(vue_esm_browser["openBlock"])(), Object(vue_esm_browser["createBlock"])("section", _hoisted_1, [_hoisted_2]);
}
// CONCATENATED MODULE: ./website/docs/zh-CN/installation.md?vue&type=template&id=4559f461

// CONCATENATED MODULE: ./website/docs/zh-CN/installation.md

const script = {}
script.render = render

/* harmony default export */ var installation = __webpack_exports__["default"] = (script);

/***/ })

}]);
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--12-0!./website/pages/component.vue?vue&type=template&id=41af3894&scoped=true


var _withId = /*#__PURE__*/Object(vue_esm_browser["withScopeId"])("data-v-41af3894");

Object(vue_esm_browser["pushScopeId"])("data-v-41af3894");

var _hoisted_1 = {
  class: "content-wrap"
};

Object(vue_esm_browser["popScopeId"])();

var render = /*#__PURE__*/_withId(function (_ctx, _cache, $props, $setup, $data, $options) {
  var _component_side_nav = Object(vue_esm_browser["resolveComponent"])("side-nav");

  var _component_el_aside = Object(vue_esm_browser["resolveComponent"])("el-aside");

  var _component_router_view = Object(vue_esm_browser["resolveComponent"])("router-view");

  var _component_el_main = Object(vue_esm_browser["resolveComponent"])("el-main");

  var _component_el_container = Object(vue_esm_browser["resolveComponent"])("el-container");

  return Object(vue_esm_browser["openBlock"])(), Object(vue_esm_browser["createBlock"])(_component_el_container, {
    class: "page-container page-component"
  }, {
    default: _withId(function () {
      return [Object(vue_esm_browser["createVNode"])(_component_el_aside, {
        class: "page-component__menu",
        width: "200px"
      }, {
        default: _withId(function () {
          return [Object(vue_esm_browser["createVNode"])(_component_side_nav, {
            data: $data.navsData[$data.lang],
            base: "/" + $data.lang + "/component"
          }, null, 8, ["data", "base"])];
        }),
        _: 1
      }), Object(vue_esm_browser["createVNode"])(_component_el_main, {
        class: "page-component__content"
      }, {
        default: _withId(function () {
          return [Object(vue_esm_browser["createVNode"])("div", _hoisted_1, [Object(vue_esm_browser["createVNode"])(_component_router_view, {
            class: "content"
          })])];
        }),
        _: 1
      })];
    }),
    _: 1
  });
});
// CONCATENATED MODULE: ./website/pages/component.vue?vue&type=template&id=41af3894&scoped=true

// EXTERNAL MODULE: ./website/bus.js
var bus = __webpack_require__(72);

// EXTERNAL MODULE: ./website/nav.config.json
var nav_config = __webpack_require__(79);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--12-0!./website/pages/component.vue?vue&type=script&lang=js
function _createForOfIteratorHelperLoose(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; return function () { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } it = o[Symbol.iterator](); return it.next.bind(it); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }



/* harmony default export */ var componentvue_type_script_lang_js = ({
  data: function data() {
    return {
      lang: this.$route.meta.lang,
      navsData: nav_config,
      scrollTop: 0,
      showHeader: true,
      componentScrollBar: null,
      componentScrollBoxElement: null
    };
  },
  computed: {
    showBackToTop: function showBackToTop() {
      return !this.$route.path.match(/backtop/);
    }
  },
  watch: {
    '$route.path': function $routePath() {// // 触发伪滚动条更新
      // this.componentScrollBox.scrollTop = 0
      // this.$nextTick(() => {
      //   this.componentScrollBar.update()
      // })
    }
  },
  created: function created() {
    var _this = this;

    bus["a" /* default */].$on('nav-fade', function (val) {
      _this.navFaded = val;
    });
  },
  mounted: function mounted() {
    // this.componentScrollBar = this.$refs.componentScrollBar
    // this.componentScrollBox = this.componentScrollBar.$el.querySelector('.el-scrollbar__wrap')
    // this.throttledScrollHandler = throttle(300, this.handleScroll)
    // this.componentScrollBox.addEventListener('scroll', this.throttledScrollHandler)
    document.body.classList.add('is-component');
    this.addContentObserver();
  },
  unmounted: function unmounted() {
    document.body.classList.remove('is-component');
  },
  beforeUnmount: function beforeUnmount() {
    this.componentScrollBox.removeEventListener('scroll', this.throttledScrollHandler);
    this.observer.disconnect();
    this.headerAnchorObserver.disconnect();
  },
  methods: {
    addContentObserver: function addContentObserver() {
      var _this2 = this;

      this.observer = new MutationObserver(function (mutationsList, observer) {
        for (var _iterator = _createForOfIteratorHelperLoose(mutationsList), _step; !(_step = _iterator()).done;) {
          var mutation = _step.value;

          if (mutation.type === 'childList') {
            _this2.renderAnchorHref();

            _this2.goAnchor();
          }
        }
      });
      this.observer.observe(document.querySelector('.content-wrap'), {
        childList: true
      }); // 标题前图标隐藏显示 START

      this.headerAnchorObserver = new MutationObserver(function (mutationsList, observer) {
        // 新换了一批 header-anchor , 再次绑定事件
        var headerAnchors = document.getElementsByClassName('header-anchor');

        var _loop = function _loop(i) {
          var elem = headerAnchors[i];
          var p = elem.parentNode;

          if (p) {
            p.onmouseover = function () {
              elem.style.opacity = 1;
            };

            p.onmouseout = function () {
              elem.style.opacity = 0;
            };
          }
        };

        for (var i = 0; i < headerAnchors.length; i++) {
          _loop(i);
        }
      });
      this.headerAnchorObserver.observe( // 监听 content-wrap 下 dom 的变化
      document.querySelector('.content-wrap'), {
        childList: true
      }); // 标题前图标隐藏显示 END
    },
    renderAnchorHref: function renderAnchorHref() {
      if (/changelog/g.test(location.href)) return;
      var anchors = document.querySelectorAll('h2 a,h3 a,h4 a,h5 a');
      var basePath = location.href.split('#').splice(0, 2).join('#');
      [].slice.call(anchors).forEach(function (a) {
        var href = a.getAttribute('href');

        if (href.indexOf('#') === 0) {
          a.href = basePath + href;
        }
      });
    },
    goAnchor: function goAnchor() {
      var _this3 = this;

      if (location.href.match(/#/g).length > 1) {
        var anchor = location.href.match(/#[^#]+$/g);
        if (!anchor) return;
        var elm = document.querySelector(anchor[0]);
        if (!elm) return;
        setTimeout(function () {
          _this3.componentScrollBox.scrollTop = elm.offsetTop;
        }, 50);
      }
    },
    handleScroll: function handleScroll() {
      var scrollTop = this.componentScrollBox.scrollTop;

      if (this.showHeader !== this.scrollTop > scrollTop) {
        this.showHeader = this.scrollTop > scrollTop;
      }

      if (scrollTop === 0) {
        this.showHeader = true;
      }

      if (!this.navFaded) {
        bus["a" /* default */].$emit('fade-nav');
      }

      this.scrollTop = scrollTop;
    }
  }
});
// CONCATENATED MODULE: ./website/pages/component.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./website/pages/component.vue?vue&type=style&index=0&id=41af3894&lang=css
var componentvue_type_style_index_0_id_41af3894_lang_css = __webpack_require__(716);

// EXTERNAL MODULE: ./website/pages/component.vue?vue&type=style&index=1&id=41af3894&lang=scss&scoped=true
var componentvue_type_style_index_1_id_41af3894_lang_scss_scoped_true = __webpack_require__(718);

// CONCATENATED MODULE: ./website/pages/component.vue






componentvue_type_script_lang_js.render = render
componentvue_type_script_lang_js.__scopeId = "data-v-41af3894"

/* harmony default export */ var component = __webpack_exports__["default"] = (componentvue_type_script_lang_js);

/***/ }),

/***/ 707:
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(7);
            var content = __webpack_require__(717);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ 708:
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(7);
            var content = __webpack_require__(719);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ 716:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_5_3_node_modules_vue_loader_dist_index_js_ref_12_0_component_vue_vue_type_style_index_0_id_41af3894_lang_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(707);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_5_3_node_modules_vue_loader_dist_index_js_ref_12_0_component_vue_vue_type_style_index_0_id_41af3894_lang_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_5_3_node_modules_vue_loader_dist_index_js_ref_12_0_component_vue_vue_type_style_index_0_id_41af3894_lang_css__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ 717:
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ 718:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_5_3_node_modules_vue_loader_dist_index_js_ref_12_0_component_vue_vue_type_style_index_1_id_41af3894_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(708);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_5_3_node_modules_vue_loader_dist_index_js_ref_12_0_component_vue_vue_type_style_index_1_id_41af3894_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_5_3_node_modules_vue_loader_dist_index_js_ref_12_0_component_vue_vue_type_style_index_1_id_41af3894_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ 719:
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

}]);
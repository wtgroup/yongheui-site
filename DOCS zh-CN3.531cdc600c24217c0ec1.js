(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ 695:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/light-button.md?vue&type=template&id=36a657f5

var light_buttonvue_type_template_id_36a657f5_hoisted_1 = {
  class: "content element-doc"
};

var light_buttonvue_type_template_id_36a657f5_hoisted_2 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("h2", {
  id: "lightbutton-qing-an-niu"
}, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("a", {
  class: "header-anchor",
  href: "#lightbutton-qing-an-niu"
}, "¶"), /*#__PURE__*/Object(vue_esm_browser["createTextVNode"])(" LightButton 轻按钮")], -1);

var light_buttonvue_type_template_id_36a657f5_hoisted_3 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("h3", {
  id: "ji-ben-yong-fa"
}, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("a", {
  class: "header-anchor",
  href: "#ji-ben-yong-fa"
}, "¶"), /*#__PURE__*/Object(vue_esm_browser["createTextVNode"])(" 基本用法")], -1);

var _hoisted_4 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("code", {
  class: "html"
}, "<template>\n  <y-light-button @click=\"clickEvent('基本按钮')\">基本按钮</y-light-button>\n  <y-light-button type=\"primary\" @click=\"clickEvent('primary')\">primary</y-light-button>\n  <y-light-button type=\"danger\" @click=\"clickEvent('danger')\">danger</y-light-button>\n</template>\n<script>\n  export default {\n    methods: {\n        clickEvent(t) {\n          console.log(t);\n      }\n    }\n  }\n</script>\n")], -1);

var _hoisted_5 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("h3", {
  id: "dai-qian-hou-zhui"
}, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("a", {
  class: "header-anchor",
  href: "#dai-qian-hou-zhui"
}, "¶"), /*#__PURE__*/Object(vue_esm_browser["createTextVNode"])(" 带前/后缀")], -1);

var _hoisted_6 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("code", {
  class: "html"
}, "<template>\n  <y-light-button @click=\"clickEvent('带前后缀')\">\n    <template v-slot:prefixIcon>🙂</template>\n    带前后缀\n    <template v-slot:suffixIcon>😂</template>\n  </y-light-button>\n</template>\n<script>\n  export default {\n    methods: {\n        clickEvent(t) {\n          console.log(t);\n      }\n    }\n  }\n</script>\n")], -1);

var _hoisted_7 = /*#__PURE__*/Object(vue_esm_browser["createStaticVNode"])("<h3 id=\"api\"><a class=\"header-anchor\" href=\"#api\">¶</a> API</h3><h4 id=\"slots\"><a class=\"header-anchor\" href=\"#slots\">¶</a> Slots</h4><table><thead><tr><th>name</th><th>说明</th></tr></thead><tbody><tr><td>—</td><td>按钮显示内容</td></tr><tr><td>prefixIcon</td><td>前缀图标</td></tr><tr><td>suffixIcon</td><td>后缀图标</td></tr></tbody></table>", 3);

function light_buttonvue_type_template_id_36a657f5_render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_element_demo0 = Object(vue_esm_browser["resolveComponent"])("element-demo0");

  var _component_demo_block = Object(vue_esm_browser["resolveComponent"])("demo-block");

  var _component_element_demo1 = Object(vue_esm_browser["resolveComponent"])("element-demo1");

  return Object(vue_esm_browser["openBlock"])(), Object(vue_esm_browser["createBlock"])("section", light_buttonvue_type_template_id_36a657f5_hoisted_1, [light_buttonvue_type_template_id_36a657f5_hoisted_2, light_buttonvue_type_template_id_36a657f5_hoisted_3, Object(vue_esm_browser["createVNode"])(_component_demo_block, null, {
    source: Object(vue_esm_browser["withCtx"])(function () {
      return [Object(vue_esm_browser["createVNode"])(_component_element_demo0)];
    }),
    highlight: Object(vue_esm_browser["withCtx"])(function () {
      return [_hoisted_4];
    }),
    _: 1
  }), _hoisted_5, Object(vue_esm_browser["createVNode"])(_component_demo_block, null, {
    source: Object(vue_esm_browser["withCtx"])(function () {
      return [Object(vue_esm_browser["createVNode"])(_component_element_demo1)];
    }),
    highlight: Object(vue_esm_browser["withCtx"])(function () {
      return [_hoisted_6];
    }),
    _: 1
  }), _hoisted_7]);
}
// CONCATENATED MODULE: ./website/docs/zh-CN/light-button.md?vue&type=template&id=36a657f5

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/extends.js
var helpers_extends = __webpack_require__(45);
var extends_default = /*#__PURE__*/__webpack_require__.n(helpers_extends);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/light-button.md?vue&type=script&lang=ts


/* harmony default export */ var light_buttonvue_type_script_lang_ts = ({
  name: 'component-doc',
  components: {
    "element-demo0": function () {
      var _createTextVNode = vue_esm_browser["createTextVNode"],
          _resolveComponent = vue_esm_browser["resolveComponent"],
          _withCtx = vue_esm_browser["withCtx"],
          _createVNode = vue_esm_browser["createVNode"],
          _openBlock = vue_esm_browser["openBlock"],
          _createBlock = vue_esm_browser["createBlock"];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("基本按钮");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("primary");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("danger");

      function render(_ctx, _cache) {
        var _component_y_light_button = _resolveComponent("y-light-button");

        return _openBlock(), _createBlock("div", null, [_createVNode(_component_y_light_button, {
          onClick: _cache[1] || (_cache[1] = function ($event) {
            return _ctx.clickEvent('基本按钮');
          })
        }, {
          default: _withCtx(function () {
            return [_hoisted_1];
          }),
          _: 1
        }), _createVNode(_component_y_light_button, {
          type: "primary",
          onClick: _cache[2] || (_cache[2] = function ($event) {
            return _ctx.clickEvent('primary');
          })
        }, {
          default: _withCtx(function () {
            return [_hoisted_2];
          }),
          _: 1
        }), _createVNode(_component_y_light_button, {
          type: "danger",
          onClick: _cache[3] || (_cache[3] = function ($event) {
            return _ctx.clickEvent('danger');
          })
        }, {
          default: _withCtx(function () {
            return [_hoisted_3];
          }),
          _: 1
        })]);
      }

      var democomponentExport = {
        methods: {
          clickEvent: function clickEvent(t) {
            console.log(t);
          }
        }
      };
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "element-demo1": function () {
      var _createTextVNode = vue_esm_browser["createTextVNode"],
          _resolveComponent = vue_esm_browser["resolveComponent"],
          _withCtx = vue_esm_browser["withCtx"],
          _createVNode = vue_esm_browser["createVNode"],
          _openBlock = vue_esm_browser["openBlock"],
          _createBlock = vue_esm_browser["createBlock"];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("🙂");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode(" 带前后缀 ");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("😂");

      function render(_ctx, _cache) {
        var _component_y_light_button = _resolveComponent("y-light-button");

        return _openBlock(), _createBlock("div", null, [_createVNode(_component_y_light_button, {
          onClick: _cache[1] || (_cache[1] = function ($event) {
            return _ctx.clickEvent('带前后缀');
          })
        }, {
          prefixIcon: _withCtx(function () {
            return [_hoisted_1];
          }),
          suffixIcon: _withCtx(function () {
            return [_hoisted_3];
          }),
          default: _withCtx(function () {
            return [_hoisted_2];
          }),
          _: 1
        })]);
      }

      var democomponentExport = {
        methods: {
          clickEvent: function clickEvent(t) {
            console.log(t);
          }
        }
      };
      return extends_default()({
        render: render
      }, democomponentExport);
    }()
  }
});
// CONCATENATED MODULE: ./website/docs/zh-CN/light-button.md?vue&type=script&lang=ts
 
// CONCATENATED MODULE: ./website/docs/zh-CN/light-button.md



light_buttonvue_type_script_lang_ts.render = light_buttonvue_type_template_id_36a657f5_render

/* harmony default export */ var light_button = __webpack_exports__["default"] = (light_buttonvue_type_script_lang_ts);

/***/ })

}]);
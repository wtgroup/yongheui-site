(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[7],{

/***/ 697:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/search-select.md?vue&type=template&id=66ed1158

var _hoisted_1 = {
  class: "content element-doc"
};

var _hoisted_2 = /*#__PURE__*/Object(vue_esm_browser["createStaticVNode"])("<h2 id=\"searchselect-sou-suo-xuan-ze-qi\"><a class=\"header-anchor\" href=\"#searchselect-sou-suo-xuan-ze-qi\">¶</a> SearchSelect 搜索选择器</h2><p>对 <code>ant-design-vue</code> <code>select</code> 的封装。 <strong>为解决普通下拉框选项过多导致渲染慢的问题。</strong> 1000个item，仅渲染就约3s，每次点击下拉框就要等待3s，频繁操作时让人抓狂。 本选择器默认只展示少量的选项集，利用滚动翻页的方式加载更多选项集，极大减少单次渲染的项数，来减少等待时长。</p><h3 id=\"ji-ben-yong-fa\"><a class=\"header-anchor\" href=\"#ji-ben-yong-fa\">¶</a> 基本用法</h3><p>基本使用和<code>ASelect</code>基本一致，选项参数方法透传。</p>", 4);

var _hoisted_6 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("code", {
  class: "html"
}, "<template>\n  <y-search-select v-model:value=\"myValue\" placeholder=\"请选择\" style=\"width: 300px\" :full-options=\"options\" />\n</template>\n<script>\n  export default {\n    data() {\n      return {\n          myValue: 3,\n          options: [\n              {value: 1, text: '张三', extra: '小张三'},\n              {value: 2, text: '李四'},\n              {value: 3, text: '王五', extra: '3'},\n              {value: 4, text: '赵六', extra: '4'},\n          ],\n      }\n    },\n  }\n</script>\n")], -1);

var _hoisted_7 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("h3", {
  id: "zi-ding-yi-xuan-xiang-xuan-ran-slot"
}, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("a", {
  class: "header-anchor",
  href: "#zi-ding-yi-xuan-xiang-xuan-ran-slot"
}, "¶"), /*#__PURE__*/Object(vue_esm_browser["createTextVNode"])(" 自定义选项渲染(slot)")], -1);

var _hoisted_8 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("code", {
  class: "html"
}, "<template>\n  <y-search-select placeholder=\"请选择\" style=\"width: 300px\" :full-options=\"options\">\n      <template v-slot=\"{value, text, extra}\">\n        {{value}} -- {{text}} -- {{extra}}\n      </template>\n  </y-search-select>\n</template>\n<script>\n  export default {\n    data() {\n      return {\n          options: [\n              {value: 1, text: '张三', extra: '小张三'},\n              {value: 2, text: '李四'},\n              {value: 3, text: '王五', extra: '3'},\n              {value: 4, text: '赵六', extra: '4'},\n          ],\n      }\n    },\n  }\n</script>\n")], -1);

var _hoisted_9 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("h3", {
  id: "xuan-xiang-fen-ye"
}, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("a", {
  class: "header-anchor",
  href: "#xuan-xiang-fen-ye"
}, "¶"), /*#__PURE__*/Object(vue_esm_browser["createTextVNode"])(" 选项分页")], -1);

var _hoisted_10 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("p", null, "解决普通下拉框选项过多导致渲染慢的问题。默认只显示第一页（前10条），向下滚动逐渐追加显示更多选项。", -1);

var _hoisted_11 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("code", {
  class: "html"
}, "<template>\n  <y-search-select placeholder=\"请选择\" style=\"width: 300px\" :full-options=\"bigOptions\"\n                   @change=\"changeEvent\"\n  />\n</template>\n<script>\n  const TAG = \"[YSearchSelect]\"\n  const mockFullOptions = () => {\n    let arr = [];\n    for (let i = 0; i < 2000; i++) {\n      arr.push({\n        value: i,\n        text: 'text' + i,\n      })\n    }\n    return arr;\n  }\n\n  export default {\n    data() {\n      return {\n          bigOptions: mockFullOptions(),\n      }\n    },\n    methods: {\n      changeEvent(args) {\n        console.log(TAG, 'changeEvent:', args);\n      }\n    }\n  }\n</script>\n")], -1);

var _hoisted_12 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("h3", {
  id: "zi-ding-yi-sou-suo"
}, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("a", {
  class: "header-anchor",
  href: "#zi-ding-yi-sou-suo"
}, "¶"), /*#__PURE__*/Object(vue_esm_browser["createTextVNode"])(" 自定义搜索")], -1);

var _hoisted_13 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["createVNode"])("code", {
  class: "html"
}, "<template>\n  <y-search-select placeholder=\"请选择\" style=\"width: 300px\" :full-options=\"options\"\n                   :search=\"mySearch\"\n  />\n</template>\n<script>\n  const TAG = \"[YSearchSelect]\"\n  export default {\n    data() {\n      return {\n          options: [\n              {value: 1, text: '张三', extra: '小张三'},\n              {value: 2, text: '李四'},\n              {value: 3, text: '王五', extra: '3'},\n              {value: 4, text: '赵六', extra: '4'},\n          ],\n      }\n    },\n    methods: {\n      mySearch({keyword,fullOptions,currentValue,pager}) {\n        console.log(TAG, {keyword, fullOptions, currentValue, pager});\n        // 这里可以请求服务端异步搜索和加载\n        return new Promise((resolve, reject) => {\n          // 模拟服务端响应\n          setTimeout(()=>{\n            const arr = [];\n            const n = Math.ceil(Math.random()*10);\n            for (let i = 0; i < n; i++) {\n              arr.push({\n                value: `${keyword}-${i}`,\n                text: `${keyword}-${i}`,\n              })\n\n            }\n            resolve(arr);\n          })\n        })\n      },\n    }\n  }\n</script>\n")], -1);

var _hoisted_14 = /*#__PURE__*/Object(vue_esm_browser["createStaticVNode"])("<h3 id=\"api\"><a class=\"header-anchor\" href=\"#api\">¶</a> API</h3><h4 id=\"attributes\"><a class=\"header-anchor\" href=\"#attributes\">¶</a> Attributes</h4><p><code>ant-design-vue</code> <code>select</code> 的属性均可用。</p><table><thead><tr><th>参数</th><th>说明</th><th>类型</th><th>可选值</th><th>默认值</th></tr></thead><tbody><tr><td>value / v-model</td><td>绑定值</td><td>boolean / string / number</td><td>—</td><td>—</td></tr><tr><td>defaultOptions</td><td>默认选项集</td><td>array / function(fullOptions: array)</td><td>true</td><td></td></tr><tr><td>defaultLimit</td><td>默认情况下展示的选项数量</td><td>number</td><td>10</td><td></td></tr><tr><td>replaceFields</td><td>选项的替换字段, 格式:<br><code>{ value:&#39;value&#39;, // 字符串对应自定义节点对象的key text:(item,index,fieldName)=&gt;{} // 支持函数取值 }</code></td><td>object</td><td></td><td>value,text,extra</td></tr><tr><td>search</td><td>定制搜索逻辑. <code>ASelect &#39;onsearch&#39;</code> 触发. 可以是获取搜索后选项的逻辑, 也可以直接传入搜索后的选项集. 函数逻辑可以同步, 可以异步(Promise). 缺省, 已有全量选项集中模糊匹配满足条件的最多一页选项集. 若果是函数, 回调参数: <code>{keyword,fullOptions,currentValue,pager}</code>, 返回匹配的选项集.</td><td>function / array</td><td></td><td></td></tr><tr><td>searchDelay</td><td>输入关键词后延迟多久执行搜索</td><td>number</td><td></td><td>500</td></tr><tr><td>enableScrollPage</td><td>是否开启滚动</td><td>boolean</td><td></td><td>true</td></tr></tbody></table><h4 id=\"events\"><a class=\"header-anchor\" href=\"#events\">¶</a> Events</h4><table><thead><tr><th>事件名称</th><th>说明</th><th>回调参数</th></tr></thead><tbody><tr><td>change</td><td>选中值发生变化时触发</td><td>目前的选中值</td></tr><tr><td>dropdown</td><td>点击下拉时触发</td><td></td></tr></tbody></table><h4 id=\"slots\"><a class=\"header-anchor\" href=\"#slots\">¶</a> Slots</h4><table><thead><tr><th>name</th><th>说明</th></tr></thead><tbody><tr><td>—</td><td>Option 组件列表(包裹在<code>a-select-option</code>内)</td></tr></tbody></table>", 8);

function search_selectvue_type_template_id_66ed1158_render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_element_demo0 = Object(vue_esm_browser["resolveComponent"])("element-demo0");

  var _component_demo_block = Object(vue_esm_browser["resolveComponent"])("demo-block");

  var _component_element_demo1 = Object(vue_esm_browser["resolveComponent"])("element-demo1");

  var _component_element_demo2 = Object(vue_esm_browser["resolveComponent"])("element-demo2");

  var _component_element_demo3 = Object(vue_esm_browser["resolveComponent"])("element-demo3");

  return Object(vue_esm_browser["openBlock"])(), Object(vue_esm_browser["createBlock"])("section", _hoisted_1, [_hoisted_2, Object(vue_esm_browser["createVNode"])(_component_demo_block, null, {
    source: Object(vue_esm_browser["withCtx"])(function () {
      return [Object(vue_esm_browser["createVNode"])(_component_element_demo0)];
    }),
    highlight: Object(vue_esm_browser["withCtx"])(function () {
      return [_hoisted_6];
    }),
    _: 1
  }), _hoisted_7, Object(vue_esm_browser["createVNode"])(_component_demo_block, null, {
    source: Object(vue_esm_browser["withCtx"])(function () {
      return [Object(vue_esm_browser["createVNode"])(_component_element_demo1)];
    }),
    highlight: Object(vue_esm_browser["withCtx"])(function () {
      return [_hoisted_8];
    }),
    _: 1
  }), _hoisted_9, _hoisted_10, Object(vue_esm_browser["createVNode"])(_component_demo_block, null, {
    source: Object(vue_esm_browser["withCtx"])(function () {
      return [Object(vue_esm_browser["createVNode"])(_component_element_demo2)];
    }),
    highlight: Object(vue_esm_browser["withCtx"])(function () {
      return [_hoisted_11];
    }),
    _: 1
  }), _hoisted_12, Object(vue_esm_browser["createVNode"])(_component_demo_block, null, {
    source: Object(vue_esm_browser["withCtx"])(function () {
      return [Object(vue_esm_browser["createVNode"])(_component_element_demo3)];
    }),
    highlight: Object(vue_esm_browser["withCtx"])(function () {
      return [_hoisted_13];
    }),
    _: 1
  }), _hoisted_14]);
}
// CONCATENATED MODULE: ./website/docs/zh-CN/search-select.md?vue&type=template&id=66ed1158

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/extends.js
var helpers_extends = __webpack_require__(45);
var extends_default = /*#__PURE__*/__webpack_require__.n(helpers_extends);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/search-select.md?vue&type=script&lang=ts


/* harmony default export */ var search_selectvue_type_script_lang_ts = ({
  name: 'component-doc',
  components: {
    "element-demo0": function () {
      var _resolveComponent = vue_esm_browser["resolveComponent"],
          _createVNode = vue_esm_browser["createVNode"],
          _openBlock = vue_esm_browser["openBlock"],
          _createBlock = vue_esm_browser["createBlock"];

      function render(_ctx, _cache) {
        var _component_y_search_select = _resolveComponent("y-search-select");

        return _openBlock(), _createBlock("div", null, [_createVNode(_component_y_search_select, {
          value: _ctx.myValue,
          "onUpdate:value": _cache[1] || (_cache[1] = function ($event) {
            return _ctx.myValue = $event;
          }),
          placeholder: "请选择",
          style: {
            "width": "300px"
          },
          "full-options": _ctx.options
        }, null, 8, ["value", "full-options"])]);
      }

      var democomponentExport = {
        data: function data() {
          return {
            myValue: 3,
            options: [{
              value: 1,
              text: '张三',
              extra: '小张三'
            }, {
              value: 2,
              text: '李四'
            }, {
              value: 3,
              text: '王五',
              extra: '3'
            }, {
              value: 4,
              text: '赵六',
              extra: '4'
            }]
          };
        }
      };
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "element-demo1": function () {
      var _toDisplayString = vue_esm_browser["toDisplayString"],
          _createTextVNode = vue_esm_browser["createTextVNode"],
          _resolveComponent = vue_esm_browser["resolveComponent"],
          _withCtx = vue_esm_browser["withCtx"],
          _createVNode = vue_esm_browser["createVNode"],
          _openBlock = vue_esm_browser["openBlock"],
          _createBlock = vue_esm_browser["createBlock"];

      function render(_ctx, _cache) {
        var _component_y_search_select = _resolveComponent("y-search-select");

        return _openBlock(), _createBlock("div", null, [_createVNode(_component_y_search_select, {
          placeholder: "请选择",
          style: {
            "width": "300px"
          },
          "full-options": _ctx.options
        }, {
          default: _withCtx(function (_ref) {
            var value = _ref.value,
                text = _ref.text,
                extra = _ref.extra;
            return [_createTextVNode(_toDisplayString(value) + " -- " + _toDisplayString(text) + " -- " + _toDisplayString(extra), 1)];
          }),
          _: 1
        }, 8, ["full-options"])]);
      }

      var democomponentExport = {
        data: function data() {
          return {
            options: [{
              value: 1,
              text: '张三',
              extra: '小张三'
            }, {
              value: 2,
              text: '李四'
            }, {
              value: 3,
              text: '王五',
              extra: '3'
            }, {
              value: 4,
              text: '赵六',
              extra: '4'
            }]
          };
        }
      };
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "element-demo2": function () {
      var _resolveComponent = vue_esm_browser["resolveComponent"],
          _createVNode = vue_esm_browser["createVNode"],
          _openBlock = vue_esm_browser["openBlock"],
          _createBlock = vue_esm_browser["createBlock"];

      function render(_ctx, _cache) {
        var _component_y_search_select = _resolveComponent("y-search-select");

        return _openBlock(), _createBlock("div", null, [_createVNode(_component_y_search_select, {
          placeholder: "请选择",
          style: {
            "width": "300px"
          },
          "full-options": _ctx.bigOptions,
          onChange: _ctx.changeEvent
        }, null, 8, ["full-options", "onChange"])]);
      }

      var TAG = "[YSearchSelect]";

      var mockFullOptions = function mockFullOptions() {
        var arr = [];

        for (var i = 0; i < 2000; i++) {
          arr.push({
            value: i,
            text: 'text' + i
          });
        }

        return arr;
      };

      var democomponentExport = {
        data: function data() {
          return {
            bigOptions: mockFullOptions()
          };
        },
        methods: {
          changeEvent: function changeEvent(args) {
            console.log(TAG, 'changeEvent:', args);
          }
        }
      };
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "element-demo3": function () {
      var _resolveComponent = vue_esm_browser["resolveComponent"],
          _createVNode = vue_esm_browser["createVNode"],
          _openBlock = vue_esm_browser["openBlock"],
          _createBlock = vue_esm_browser["createBlock"];

      function render(_ctx, _cache) {
        var _component_y_search_select = _resolveComponent("y-search-select");

        return _openBlock(), _createBlock("div", null, [_createVNode(_component_y_search_select, {
          placeholder: "请选择",
          style: {
            "width": "300px"
          },
          "full-options": _ctx.options,
          search: _ctx.mySearch
        }, null, 8, ["full-options", "search"])]);
      }

      var TAG = "[YSearchSelect]";
      var democomponentExport = {
        data: function data() {
          return {
            options: [{
              value: 1,
              text: '张三',
              extra: '小张三'
            }, {
              value: 2,
              text: '李四'
            }, {
              value: 3,
              text: '王五',
              extra: '3'
            }, {
              value: 4,
              text: '赵六',
              extra: '4'
            }]
          };
        },
        methods: {
          mySearch: function mySearch(_ref2) {
            var keyword = _ref2.keyword,
                fullOptions = _ref2.fullOptions,
                currentValue = _ref2.currentValue,
                pager = _ref2.pager;
            console.log(TAG, {
              keyword: keyword,
              fullOptions: fullOptions,
              currentValue: currentValue,
              pager: pager
            }); // 这里可以请求服务端异步搜索和加载

            return new Promise(function (resolve, reject) {
              // 模拟服务端响应
              setTimeout(function () {
                var arr = [];
                var n = Math.ceil(Math.random() * 10);

                for (var i = 0; i < n; i++) {
                  arr.push({
                    value: keyword + "-" + i,
                    text: keyword + "-" + i
                  });
                }

                resolve(arr);
              });
            });
          }
        }
      };
      return extends_default()({
        render: render
      }, democomponentExport);
    }()
  }
});
// CONCATENATED MODULE: ./website/docs/zh-CN/search-select.md?vue&type=script&lang=ts
 
// CONCATENATED MODULE: ./website/docs/zh-CN/search-select.md



search_selectvue_type_script_lang_ts.render = search_selectvue_type_template_id_66ed1158_render

/* harmony default export */ var search_select = __webpack_exports__["default"] = (search_selectvue_type_script_lang_ts);

/***/ })

}]);
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--12-0!./website/pages/changelog.vue?vue&type=template&id=25d67d23

var _hoisted_1 = {
  class: "page-changelog"
};
var _hoisted_2 = {
  class: "heading"
};

var _hoisted_3 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("a", {
  href: "https://github.com/element-plus/element-plus/releases",
  target: "_blank"
}, "GitHub Releases", -1);

var _hoisted_4 = {
  ref: "timeline",
  class: "timeline"
};

var _hoisted_5 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("div", null, "Change Log 位置(todel)", -1);

function render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_el_button = Object(vue_esm_browser["resolveComponent"])("el-button");

  return Object(vue_esm_browser["openBlock"])(), Object(vue_esm_browser["createBlock"])("div", _hoisted_1, [Object(vue_esm_browser["createVNode"])("div", _hoisted_2, [Object(vue_esm_browser["createVNode"])(_component_el_button, {
    class: "fr"
  }, {
    default: Object(vue_esm_browser["withCtx"])(function () {
      return [_hoisted_3];
    }),
    _: 1
  }), Object(vue_esm_browser["createTextVNode"])(" " + Object(vue_esm_browser["toDisplayString"])($options.langConfig[1]), 1)]), Object(vue_esm_browser["createVNode"])("ul", _hoisted_4, null, 512), _hoisted_5]);
}
// CONCATENATED MODULE: ./website/pages/changelog.vue?vue&type=template&id=25d67d23

// EXTERNAL MODULE: ./website/i18n/page.json
var page = __webpack_require__(705);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--12-0!./website/pages/changelog.vue?vue&type=script&lang=js
// import ChangeLogCn from '../../CHANGELOG.zh-CN.md'
// import ChangeLogEs from '../../CHANGELOG.es.md'
// import ChangeLogEn from '../../CHANGELOG.en-US.md'
// import ChangeLogFr from '../../CHANGELOG.fr-FR.md'
// import ChangeLogJp from '../../CHANGELOG.jp.md'

/* harmony default export */ var changelogvue_type_script_lang_js = ({
  components: {
    ChangeLogCn: ChangeLogCn // ChangeLogEs,
    // ChangeLogEn,
    // ChangeLogFr,
    // ChangeLogJp,

  },
  data: function data() {
    return {
      lang: this.$route.meta.lang
    };
  },
  computed: {
    langConfig: function langConfig() {
      var _this = this;

      return page.filter(function (config) {
        return config.lang === _this.lang;
      })[0].pages.changelog;
    }
  },
  mounted: function mounted() {
    var changeLog = this.$refs.changeLog;
    var changeLogNodes = changeLog.$el.children;
    var a = changeLogNodes[1].querySelector('a');
    a && a.remove();
    var release = changeLogNodes[1].textContent.trim();
    var fragments = "<li><h3><a href=\"https://github.com/element-plus/element-plus/releases/tag/v" + release + "\" target=\"_blank\">" + release + "</a></h3>";

    for (var len = changeLogNodes.length, i = 2; i < len; i++) {
      var node = changeLogNodes[i];
      a = changeLogNodes[i].querySelector('a');
      a && a.getAttribute('class') === 'header-anchor' && a.remove();

      if (node.tagName !== 'H3') {
        fragments += changeLogNodes[i].outerHTML;
      } else {
        release = changeLogNodes[i].textContent.trim();
        fragments += "</li><li><h3><a href=\"https://github.com/element-plus/element-plus/releases/tag/v" + release + "\" target=\"_blank\">" + release + "</a></h3>";
      }
    }

    fragments = fragments.replace(/#(\d+)/g, '<a href="https://github.com/element-plus/element-plus/issues/$1" target="_blank">#$1</a>');
    fragments = fragments.replace(/@([\w-]+)/g, '<a href="https://github.com/$1" target="_blank">@$1</a>');
    this.$refs.timeline.innerHTML = fragments + "</li>";
    changeLog.$el.remove();
  }
});
// CONCATENATED MODULE: ./website/pages/changelog.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./website/pages/changelog.vue?vue&type=style&index=0&id=25d67d23&lang=scss
var changelogvue_type_style_index_0_id_25d67d23_lang_scss = __webpack_require__(714);

// CONCATENATED MODULE: ./website/pages/changelog.vue





changelogvue_type_script_lang_js.render = render

/* harmony default export */ var changelog = __webpack_exports__["default"] = (changelogvue_type_script_lang_js);

/***/ }),

/***/ 706:
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(7);
            var content = __webpack_require__(715);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ 714:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_5_3_node_modules_vue_loader_dist_index_js_ref_12_0_changelog_vue_vue_type_style_index_0_id_25d67d23_lang_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(706);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_5_3_node_modules_vue_loader_dist_index_js_ref_12_0_changelog_vue_vue_type_style_index_0_id_25d67d23_lang_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_5_3_node_modules_vue_loader_dist_index_js_ref_12_0_changelog_vue_vue_type_style_index_0_id_25d67d23_lang_scss__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ 715:
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

}]);
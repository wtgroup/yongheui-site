(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[13],{

/***/ 686:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./website/assets/images/theme-index-blue.png
/* harmony default export */ var theme_index_blue = (__webpack_require__.p + "static/theme-index-blue.8fbb67d.png");
// CONCATENATED MODULE: ./website/assets/images/theme-index-red.png
/* harmony default export */ var theme_index_red = (__webpack_require__.p + "static/theme-index-red.5e43266.png");
// CONCATENATED MODULE: ./website/assets/images/duohui.svg
/* harmony default export */ var duohui = ("data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iOTgiIGhlaWdodD0iMTUwIiB2aWV3Qm94PSIwIDAgOTggMTUwIiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPg0KPHRpdGxlPmR1b2h1aS1lbGVtZW50PC90aXRsZT4NCjxkZXNjPkNyZWF0ZWQgdXNpbmcgRmlnbWE8L2Rlc2M+DQo8ZyBpZD0iQ2FudmFzIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMjIxNiAxNDApIj4NCjxjbGlwUGF0aCBpZD0iY2xpcC0wIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiPg0KPHBhdGggZD0iTSAyMjE2IC0xNDBMIDIzMTQgLTE0MEwgMjMxNCAxMEwgMjIxNiAxMEwgMjIxNiAtMTQwWiIgZmlsbD0iI0ZGRkZGRiIvPg0KPC9jbGlwUGF0aD4NCjxnIGlkPSJkdW9odWktZWxlbWVudCIgY2xpcC1wYXRoPSJ1cmwoI2NsaXAtMCkiPg0KPHBhdGggZD0iTSAyMjE2IC0xNDBMIDIzMTQgLTE0MEwgMjMxNCAxMEwgMjIxNiAxMEwgMjIxNiAtMTQwWiIgZmlsbD0iI0ZGRkZGRiIvPg0KPGcgaWQ9IkR1b2h1aSBJY29uIDIiPg0KPGcgaWQ9IlZlY3RvciI+DQo8dXNlIHhsaW5rOmhyZWY9IiNwYXRoMF9maWxsIiB0cmFuc2Zvcm09Im1hdHJpeCgxLjI2NjM2IDAgMCAxLjIxOTI5IDIyMzAgLTc1LjcwMzgpIiBmaWxsPSIjM0E4OEZEIi8+DQo8L2c+DQo8ZyBpZD0iVmVjdG9yIj4NCjx1c2UgeGxpbms6aHJlZj0iI3BhdGgxX2ZpbGwiIHRyYW5zZm9ybT0ibWF0cml4KDEuNDE1MzQgMCAwIDEuMjE4MzQgMjIyNiAtMTEyKSIgZmlsbD0iIzM1QUZGQiIvPg0KPC9nPg0KPC9nPg0KPC9nPg0KPC9nPg0KPGRlZnM+DQo8cGF0aCBpZD0icGF0aDBfZmlsbCIgZmlsbC1ydWxlPSJldmVub2RkIiBkPSJNIDI1Ljc3MjIgMS4wMDYzNUMgMjYuMiAwLjM3NDk4MyAyNi44OTQ5IC0yLjY2MTNlLTA3IDI3LjYzNDkgLTIuNjYxM2UtMDdDIDI4LjM3NTUgLTIuNjYxM2UtMDcgMjkuMDY5OSAwLjM3NDk4MyAyOS40OTgzIDEuMDA2MzVDIDM0LjQwOSA4LjI0OTY0IDQ3Ljc5OCAyNy45OTY0IDU0LjU5ODYgMzguMDI2MkMgNTUuMzg3OCAzOS4xOTAxIDU1LjQ4OTYgNDAuNzE2MiA1NC44NjIzIDQxLjk4M0MgNTQuMjM1IDQzLjI0OTggNTIuOTgzMyA0NC4wNDUyIDUxLjYxNzggNDQuMDQ1MkMgMzkuNjY5NCA0NC4wNDUyIDE1LjYwMSA0NC4wNDUyIDMuNjUyMSA0NC4wNDUyQyAyLjI4NzEyIDQ0LjA0NTIgMS4wMzU0NSA0My4yNDk4IDAuNDA4MTczIDQxLjk4M0MgLTAuMjE5MTA2IDQwLjcxNjIgLTAuMTE3MzUzIDM5LjE5MDEgMC42NzE4MDQgMzguMDI2MkMgNy40NzI0MiAyNy45OTY0IDIwLjg2MDkgOC4yNDk2NCAyNS43NzIyIDEuMDA2MzVaIi8+DQo8cGF0aCBpZD0icGF0aDFfZmlsbCIgZmlsbC1ydWxlPSJldmVub2RkIiBkPSJNIDI0Ljk5MzkgMS40NDcwM0MgMjUuNjEzNyAwLjUzMzExNyAyNi41NTcyIC0zLjAxNjE0ZS0wNyAyNy41NTUxIC0zLjAxNjE0ZS0wN0MgMjguNTUzNSAtMy4wMTYxNGUtMDcgMjkuNDk3IDAuNTMzMTE3IDMwLjExNjggMS40NDcwM0MgMzUuNDY3NCA5LjMzODU0IDQ3Ljg3MjUgMjcuNjM0MiA1NC40MDQzIDM3LjI2ODFDIDU1LjE4MjUgMzguNDE1NyA1NS4zMjk5IDM5Ljk4MiA1NC43ODQ3IDQxLjI5OTlDIDU0LjIzODkgNDIuNjE4NSA1My4wOTYgNDMuNDU1NiA1MS44NDI2IDQzLjQ1NTZDIDM5LjkwMjMgNDMuNDU1NiAxNS4yMDg0IDQzLjQ1NTYgMy4yNjgxMSA0My40NTU2QyAyLjAxNDcxIDQzLjQ1NTYgMC44NzE3MzkgNDIuNjE4NSAwLjMyNTk3OCA0MS4yOTk5QyAtMC4yMTk3ODMgMzkuOTgyIC0wLjA3MTc4MTMgMzguNDE1NyAwLjcwNjM5IDM3LjI2ODFDIDcuMjM4MTggMjcuNjM0MiAxOS42NDMzIDkuMzM4NTQgMjQuOTkzOSAxLjQ0NzAzWiIvPg0KPC9kZWZzPg0KPC9zdmc+DQo=");
// CONCATENATED MODULE: ./website/assets/images/bit.svg
/* harmony default export */ var bit = ("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgd2lkdGg9IjM1cHgiIGhlaWdodD0iMjJweCIgdmlld0JveD0iMCAwIDM1IDIyIiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPg0KICAgIDwhLS0gR2VuZXJhdG9yOiBTa2V0Y2ggNTEuMiAoNTc1MTkpIC0gaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoIC0tPg0KICAgIDx0aXRsZT5sb2dvX2JpdDwvdGl0bGU+DQogICAgPGRlc2M+Q3JlYXRlZCB3aXRoIFNrZXRjaC48L2Rlc2M+DQogICAgPGRlZnM+DQogICAgICAgIDxsaW5lYXJHcmFkaWVudCB4MT0iNTAlIiB5MT0iMCUiIHgyPSI1MCUiIHkyPSI5OS4wMjkwODI0JSIgaWQ9ImxpbmVhckdyYWRpZW50LTEiPg0KICAgICAgICAgICAgPHN0b3Agc3RvcC1jb2xvcj0iIzczMzk4RCIgb2Zmc2V0PSIwJSI+PC9zdG9wPg0KICAgICAgICAgICAgPHN0b3Agc3RvcC1jb2xvcj0iIzU5NEE5NSIgb2Zmc2V0PSIxMDAlIj48L3N0b3A+DQogICAgICAgIDwvbGluZWFyR3JhZGllbnQ+DQogICAgICAgIDxwYXRoIGQ9Ik0yNi42MTk2MzYxLDE4LjIwNjk0NzYgQzI2LjYxOTYzNjEsMjIuNjY3MDM4OCAyMy45MzI5Njc1LDI1LjUzODYwNDQgMTkuMTE1NDkyNywyNS41Mzg2MDQ0IEMxNy4zODYxNDI4LDI1LjUzODYwNDQgMTUuNzgwMzE3OSwyNS4yMDI1NzAxIDE0LjU0NTA2OCwyNC40OTk5NTMgQzEzLjEyNDUzMDYsMjMuNzA1NjkwMiAxMy4xMjQ1MzA2LDIyLjY5NzU4NzQgMTMuMTI0NTMwNiwyMS42NTg5MzYgTDEzLjEyNDUzMDYsNi4yMzE5MDgyNCBDMTMuMTI0NTMwNiw1LjEzMjE1OTczIDEzLjgwMzkxOCw0LjU4MjI4NTQ3IDE1LjE5MzU3NDIsNC41ODIyODU0NyBDMTYuNTgzMjMwNCw0LjU4MjI4NTQ3IDE3LjI2MjYxNzgsNS4xMzIxNTk3MyAxNy4yNjI2MTc4LDYuMjMxOTA4MjQgTDE3LjI2MjYxNzgsMTIuMDk3MjMzNiBDMTguMDM0NjQ5LDExLjU3NzkwOCAxOS4wNTM3MzAyLDExLjMzMzUxOTQgMjAuMjI3MjE3NywxMS4zMzM1MTk0IEMyMy45OTQ3MywxMS4zMzM1MTk0IDI2LjYxOTYzNjEsMTMuODA3OTUzNiAyNi42MTk2MzYxLDE4LjIwNjk0NzYgWiBNMTcuMjYyNjE3OCwyMS43ODExMzAzIEMxNy43ODc1OTkxLDIyLjA4NjYxNiAxOC40MzYxMDUzLDIyLjIzOTM1ODggMTkuMTQ2Mzc0LDIyLjIzOTM1ODggQzIxLjE1MzY1NTEsMjIuMjM5MzU4OCAyMi4zODg5MDUxLDIwLjg5NTIyMTggMjIuMzg4OTA1MSwxOC40ODE4ODQ3IEMyMi4zODg5MDUxLDE2LjAzNzk5OTIgMjEuMTIyNzczOSwxNC42MzI3NjQ5IDE5LjIwODEzNjUsMTQuNjMyNzY0OSBDMTguNDY2OTg2NSwxNC42MzI3NjQ5IDE3Ljc4NzU5OTEsMTQuODQ2NjA0OSAxNy4yNjI2MTc4LDE1LjI0MzczNjMgTDE3LjI2MjYxNzgsMjEuNzgxMTMwMyBaIE0zMy45NjkzNzMyLDcuMzAxMTA4MTggQzMzLjk2OTM3MzIsOC43MDYzNDI0IDMyLjk1MDI5Miw5LjY4Mzg5NjYzIDMxLjUyOTc1NDYsOS42ODM4OTY2MyBDMzAuMTA5MjE3Miw5LjY4Mzg5NjYzIDI5LjA5MDEzNiw4LjcwNjM0MjQgMjkuMDkwMTM2LDcuMzAxMTA4MTggQzI5LjA5MDEzNiw1Ljg5NTg3Mzk3IDMwLjEwOTIxNzIsNC45NDg4NjgzMSAzMS41Mjk3NTQ2LDQuOTQ4ODY4MzEgQzMyLjk1MDI5Miw0Ljk0ODg2ODMxIDMzLjk2OTM3MzIsNS44OTU4NzM5NyAzMy45NjkzNzMyLDcuMzAxMTA4MTggWiBNMzMuNTk4Nzk4MiwxMi45ODMxNDIyIEwzMy41OTg3OTgyLDIzLjgyNzg4NDUgQzMzLjU5ODc5ODIsMjQuOTI3NjMzIDMyLjkxOTQxMDgsMjUuNDc3NTA3MiAzMS41Mjk3NTQ2LDI1LjQ3NzUwNzIgQzMwLjE0MDA5ODQsMjUuNDc3NTA3MiAyOS40NjA3MTA5LDI0LjkyNzYzMyAyOS40NjA3MTA5LDIzLjgyNzg4NDUgTDI5LjQ2MDcxMDksMTIuOTgzMTQyMiBDMjkuNDYwNzEwOSwxMS44ODMzOTM3IDMwLjE0MDA5ODQsMTEuMzMzNTE5NCAzMS41Mjk3NTQ2LDExLjMzMzUxOTQgQzMyLjkxOTQxMDgsMTEuMzMzNTE5NCAzMy41OTg3OTgyLDExLjg4MzM5MzcgMzMuNTk4Nzk4MiwxMi45ODMxNDIyIFogTTQ1LjczNTEyODgsMjEuODExNjc4OCBDNDYuODc3NzM1LDIxLjgxMTY3ODggNDcuMjE3NDI4NywyMy4wMDMwNzMxIDQ3LjIxNzQyODcsMjMuNjc1MTQxNiBDNDcuMjE3NDI4NywyNC4xMDI4MjE2IDQ3LjA2MzAyMjUsMjQuNTMwNTAxNiA0Ni40NzYyNzg4LDI0Ljg2NjUzNTggQzQ1Ljc5Njg5MTMsMjUuMjYzNjY3MiA0NC42MjM0MDM5LDI1LjUzODYwNDQgNDMuNDE5MDM1MiwyNS41Mzg2MDQ0IEM0MS4xOTU1ODUzLDI1LjUzODYwNDQgMzkuODY3NjkxNiwyNC42NTI2OTU4IDM5LjE4ODMwNDIsMjMuNDMwNzUzIEMzOC41NzA2NzkyLDIyLjMzMTAwNDUgMzguNTA4OTE2NywyMS4wMTc0MTYgMzguNTA4OTE2NywxOS42MTIxODE4IEwzOC41MDg5MTY3LDE1LjAyOTg5NjMgTDM3LjQyODA3MywxNS4wMjk4OTYzIEMzNi4zMTYzNDgxLDE1LjAyOTg5NjMgMzUuNzYwNDg1NiwxNC40ODAwMjIxIDM1Ljc2MDQ4NTYsMTMuMzE5MTc2NCBDMzUuNzYwNDg1NiwxMi4xNTgzMzA4IDM2LjMxNjM0ODEsMTEuNjA4NDU2NSAzNy40MjgwNzMsMTEuNjA4NDU2NSBMMzguNTA4OTE2NywxMS42MDg0NTY1IEwzOC41MDg5MTY3LDguOTIwMTgyMzggQzM4LjUwODkxNjcsNy44MjA0MzM4NyAzOS4xODgzMDQyLDcuMjcwNTU5NjEgNDAuNTc3OTYwNCw3LjI3MDU1OTYxIEM0MS45Njc2MTY1LDcuMjcwNTU5NjEgNDIuNjQ3MDA0LDcuODIwNDMzODcgNDIuNjQ3MDA0LDguOTIwMTgyMzggTDQyLjY0NzAwNCwxMS42MDg0NTY1IEw0NS4yNDEwMjg5LDExLjYwODQ1NjUgQzQ2LjM1Mjc1MzgsMTEuNjA4NDU2NSA0Ni45MDg2MTYzLDEyLjE1ODMzMDggNDYuOTA4NjE2MywxMy4zMTkxNzY0IEM0Ni45MDg2MTYzLDE0LjQ4MDAyMjEgNDYuMzUyNzUzOCwxNS4wMjk4OTYzIDQ1LjI0MTAyODksMTUuMDI5ODk2MyBMNDIuNjQ3MDA0LDE1LjAyOTg5NjMgTDQyLjY0NzAwNCwxOS42MTIxODE4IEM0Mi42NDcwMDQsMjAuMTYyMDU2MSA0Mi42NDcwMDQsMjAuOTU2MzE4OSA0Mi45MjQ5MzUyLDIxLjQ3NTY0NDYgQzQzLjE0MTEwNCwyMS45MDMzMjQ2IDQzLjU0MjU2MDIsMjIuMDg2NjE2IDQ0LjA2NzU0MTQsMjIuMDg2NjE2IEM0NC40MDcyMzUxLDIyLjA4NjYxNiA0NC43NDY5Mjg5LDIxLjk5NDk3MDMgNDUuMDI0ODYwMSwyMS45MzM4NzMxIEM0NS4yNzE5MTAxLDIxLjg3Mjc3NiA0NS40ODgwNzg4LDIxLjgxMTY3ODggNDUuNzM1MTI4OCwyMS44MTE2Nzg4IFoiIGlkPSJwYXRoLTIiPjwvcGF0aD4NCiAgICA8L2RlZnM+DQogICAgPGcgaWQ9IkRlc2lnbiIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+DQogICAgICAgIDxnIGlkPSJTdGF0ZXMiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMTYxLjAwMDAwMCwgLTI2MC4wMDAwMDApIj4NCiAgICAgICAgICAgIDxnIGlkPSJuYXYiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDc2OC4wMDAwMDAsIDIyNC4wMDAwMDApIj4NCiAgICAgICAgICAgICAgICA8ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSg0Ny4wMDAwMDAsIDMyLjAwMDAwMCkiIGlkPSJMb2dvcy0vLWJpdF9sb2dvLXByZXNzZWQiPg0KICAgICAgICAgICAgICAgICAgICA8ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgzMzMuMDAwMDAwLCAwLjAwMDAwMCkiPg0KICAgICAgICAgICAgICAgICAgICAgICAgPGcgaWQ9ImJpdCI+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVzZSBmaWxsPSIjMTEyMjJEIiB4bGluazpocmVmPSIjcGF0aC0yIj48L3VzZT4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dXNlIGZpbGw9ImJsYWNrIiB4bGluazpocmVmPSIjcGF0aC0yIj48L3VzZT4NCiAgICAgICAgICAgICAgICAgICAgICAgIDwvZz4NCiAgICAgICAgICAgICAgICAgICAgPC9nPg0KICAgICAgICAgICAgICAgIDwvZz4NCiAgICAgICAgICAgIDwvZz4NCiAgICAgICAgPC9nPg0KICAgIDwvZz4NCjwvc3ZnPg==");
// CONCATENATED MODULE: ./website/assets/images/guide.png
/* harmony default export */ var guide = (__webpack_require__.p + "static/guide.e81dcf2.png");
// CONCATENATED MODULE: ./website/assets/images/component.png
/* harmony default export */ var component = (__webpack_require__.p + "static/component.7d1ca06.png");
// CONCATENATED MODULE: ./website/assets/images/resource.png
/* harmony default export */ var resource = (__webpack_require__.p + "static/resource.53cc5d2.png");
// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--12-0!./website/pages/index.vue?vue&type=template&id=9337db36&scoped=true









var _withId = /*#__PURE__*/Object(vue_esm_browser["withScopeId"])("data-v-9337db36");

Object(vue_esm_browser["pushScopeId"])("data-v-9337db36");

var _hoisted_1 = {
  class: "banner"
};
var _hoisted_2 = {
  class: "banner-desc"
};
var _hoisted_3 = {
  ref: "indexMainImg",
  class: "jumbotron"
};

var _hoisted_4 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("img", {
  src: theme_index_blue,
  alt: ""
}, null, -1);

var _hoisted_5 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("img", {
  src: theme_index_red,
  alt: ""
}, null, -1);

var _hoisted_6 = {
  class: "sponsors"
};
var _hoisted_7 = {
  class: "sponsor",
  href: "https://www.duohui.cn/?utm_source=element&utm_medium=web&utm_campaign=element-index",
  target: "_blank"
};

var _hoisted_8 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("img", {
  width: "45",
  src: duohui,
  alt: "duohui"
}, null, -1);

var _hoisted_9 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("span", {
  class: "name"
}, "多会", -1);

var _hoisted_10 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("p", null, "活动服务销售平台", -1);

var _hoisted_11 = {
  class: "sponsor",
  href: "https://bit.dev/?from=element-ui",
  target: "_blank"
};

var _hoisted_12 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("img", {
  width: "45",
  src: bit,
  alt: "bit"
}, null, -1);

var _hoisted_13 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("span", {
  class: "name"
}, "bit", -1);

var _hoisted_14 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("p", null, "Share Code", -1);

var _hoisted_15 = {
  class: "cards"
};
var _hoisted_16 = {
  class: "container"
};
var _hoisted_17 = {
  class: "card"
};

var _hoisted_18 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("img", {
  src: guide,
  alt: ""
}, null, -1);

var _hoisted_19 = {
  class: "card"
};

var _hoisted_20 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("img", {
  src: component,
  alt: ""
}, null, -1);

var _hoisted_21 = {
  class: "card"
};

var _hoisted_22 = /*#__PURE__*/Object(vue_esm_browser["createVNode"])("img", {
  src: resource,
  alt: ""
}, null, -1);

Object(vue_esm_browser["popScopeId"])();

var render = /*#__PURE__*/_withId(function (_ctx, _cache, $props, $setup, $data, $options) {
  var _component_router_link = Object(vue_esm_browser["resolveComponent"])("router-link");

  return Object(vue_esm_browser["openBlock"])(), Object(vue_esm_browser["createBlock"])("div", null, [Object(vue_esm_browser["createVNode"])("div", _hoisted_1, [Object(vue_esm_browser["createVNode"])("div", _hoisted_2, [Object(vue_esm_browser["createVNode"])("h1", null, Object(vue_esm_browser["toDisplayString"])($options.langConfig[1]), 1), Object(vue_esm_browser["createVNode"])("p", null, Object(vue_esm_browser["toDisplayString"])($options.langConfig[2]), 1)])]), Object(vue_esm_browser["createVNode"])("div", _hoisted_3, [_hoisted_4, Object(vue_esm_browser["createVNode"])("div", {
    class: "jumbotron-red",
    style: {
      height: $data.mainImgOffset + 'px'
    }
  }, [_hoisted_5], 4)], 512), Object(vue_esm_browser["createVNode"])("div", _hoisted_6, [Object(vue_esm_browser["createVNode"])("a", _hoisted_7, [_hoisted_8, Object(vue_esm_browser["createVNode"])("div", null, [Object(vue_esm_browser["createVNode"])("p", null, [Object(vue_esm_browser["createTextVNode"])(Object(vue_esm_browser["toDisplayString"])($options.sponsorLabel) + " ", 1), _hoisted_9]), _hoisted_10])]), Object(vue_esm_browser["createVNode"])("a", _hoisted_11, [_hoisted_12, Object(vue_esm_browser["createVNode"])("div", null, [Object(vue_esm_browser["createVNode"])("p", null, [Object(vue_esm_browser["createTextVNode"])(Object(vue_esm_browser["toDisplayString"])($options.sponsorLabel) + " ", 1), _hoisted_13]), _hoisted_14])])]), Object(vue_esm_browser["createVNode"])("div", _hoisted_15, [Object(vue_esm_browser["createVNode"])("ul", _hoisted_16, [Object(vue_esm_browser["createVNode"])("li", null, [Object(vue_esm_browser["createVNode"])("div", _hoisted_17, [_hoisted_18, Object(vue_esm_browser["createVNode"])("h3", null, Object(vue_esm_browser["toDisplayString"])($options.langConfig[3]), 1), Object(vue_esm_browser["createVNode"])("p", null, Object(vue_esm_browser["toDisplayString"])($options.langConfig[4]), 1), Object(vue_esm_browser["createVNode"])(_component_router_link, {
    "active-class": "active",
    to: "/" + $data.lang + "/guide/design",
    exact: ""
  }, {
    default: _withId(function () {
      return [Object(vue_esm_browser["createTextVNode"])(Object(vue_esm_browser["toDisplayString"])($options.langConfig[5]), 1)];
    }),
    _: 1
  }, 8, ["to"])])]), Object(vue_esm_browser["createVNode"])("li", null, [Object(vue_esm_browser["createVNode"])("div", _hoisted_19, [_hoisted_20, Object(vue_esm_browser["createVNode"])("h3", null, Object(vue_esm_browser["toDisplayString"])($options.langConfig[6]), 1), Object(vue_esm_browser["createVNode"])("p", null, Object(vue_esm_browser["toDisplayString"])($options.langConfig[7]), 1), Object(vue_esm_browser["createVNode"])(_component_router_link, {
    "active-class": "active",
    to: "/" + $data.lang + "/component/layout",
    exact: ""
  }, {
    default: _withId(function () {
      return [Object(vue_esm_browser["createTextVNode"])(Object(vue_esm_browser["toDisplayString"])($options.langConfig[5]), 1)];
    }),
    _: 1
  }, 8, ["to"])])]), Object(vue_esm_browser["createVNode"])("li", null, [Object(vue_esm_browser["createVNode"])("div", _hoisted_21, [_hoisted_22, Object(vue_esm_browser["createVNode"])("h3", null, Object(vue_esm_browser["toDisplayString"])($options.langConfig[8]), 1), Object(vue_esm_browser["createVNode"])("p", null, Object(vue_esm_browser["toDisplayString"])($options.langConfig[9]), 1), Object(vue_esm_browser["createVNode"])(_component_router_link, {
    "active-class": "active",
    to: "/" + $data.lang + "/resource",
    exact: ""
  }, {
    default: _withId(function () {
      return [Object(vue_esm_browser["createTextVNode"])(Object(vue_esm_browser["toDisplayString"])($options.langConfig[5]), 1)];
    }),
    _: 1
  }, 8, ["to"])])])])])]);
});
// CONCATENATED MODULE: ./website/pages/index.vue?vue&type=template&id=9337db36&scoped=true

// EXTERNAL MODULE: ./website/i18n/page.json
var page = __webpack_require__(705);

// EXTERNAL MODULE: ./node_modules/throttle-debounce/index.umd.js
var index_umd = __webpack_require__(724);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--12-0!./website/pages/index.vue?vue&type=script&lang=js


/* harmony default export */ var pagesvue_type_script_lang_js = ({
  data: function data() {
    return {
      lang: this.$route.meta.lang,
      mainImgOffset: 0
    };
  },
  computed: {
    sponsorLabel: function sponsorLabel() {
      return this.lang === 'zh-CN' ? '赞助商' : 'Sponsored by';
    },
    langConfig: function langConfig() {
      var _this = this;

      return page.filter(function (config) {
        return config.lang === _this.lang;
      })[0].pages.index;
    }
  },
  created: function created() {
    var _this2 = this;

    this.throttledHandleScroll = Object(index_umd["throttle"])(10, true, function (index) {
      _this2.handleScroll(index);
    });
  },
  beforeUnmount: function beforeUnmount() {
    window.removeEventListener('scroll', this.throttledHandleScroll);
  },
  mounted: function mounted() {
    window.addEventListener('scroll', this.throttledHandleScroll);
  },
  methods: {
    handleScroll: function handleScroll() {
      var ele = this.$refs.indexMainImg;
      var rect = ele.getBoundingClientRect();
      var eleHeight = ele.clientHeight + 55;
      var calHeight = (180 - rect.top) * 2;
      if (calHeight < 0) calHeight = 0;
      if (calHeight > eleHeight) calHeight = eleHeight;
      this.mainImgOffset = calHeight;
    }
  }
});
// CONCATENATED MODULE: ./website/pages/index.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./website/pages/index.vue?vue&type=style&index=0&id=9337db36&lang=scss&scoped=true
var pagesvue_type_style_index_0_id_9337db36_lang_scss_scoped_true = __webpack_require__(725);

// CONCATENATED MODULE: ./website/pages/index.vue





pagesvue_type_script_lang_js.render = render
pagesvue_type_script_lang_js.__scopeId = "data-v-9337db36"

/* harmony default export */ var pages = __webpack_exports__["default"] = (pagesvue_type_script_lang_js);

/***/ }),

/***/ 711:
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(7);
            var content = __webpack_require__(726);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ 724:
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? factory(exports) :
	undefined;
}(this, (function (exports) { 'use strict';

	/* eslint-disable no-undefined,no-param-reassign,no-shadow */

	/**
	 * Throttle execution of a function. Especially useful for rate limiting
	 * execution of handlers on events like resize and scroll.
	 *
	 * @param  {number}    delay -          A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250 (or even higher) are most useful.
	 * @param  {boolean}   [noTrailing] -   Optional, defaults to false. If noTrailing is true, callback will only execute every `delay` milliseconds while the
	 *                                    throttled-function is being called. If noTrailing is false or unspecified, callback will be executed one final time
	 *                                    after the last throttled-function call. (After the throttled-function has not been called for `delay` milliseconds,
	 *                                    the internal counter is reset).
	 * @param  {Function}  callback -       A function to be executed after delay milliseconds. The `this` context and all arguments are passed through, as-is,
	 *                                    to `callback` when the throttled-function is executed.
	 * @param  {boolean}   [debounceMode] - If `debounceMode` is true (at begin), schedule `clear` to execute after `delay` ms. If `debounceMode` is false (at end),
	 *                                    schedule `callback` to execute after `delay` ms.
	 *
	 * @returns {Function}  A new, throttled, function.
	 */
	function throttle (delay, noTrailing, callback, debounceMode) {
	  /*
	   * After wrapper has stopped being called, this timeout ensures that
	   * `callback` is executed at the proper times in `throttle` and `end`
	   * debounce modes.
	   */
	  var timeoutID;
	  var cancelled = false; // Keep track of the last time `callback` was executed.

	  var lastExec = 0; // Function to clear existing timeout

	  function clearExistingTimeout() {
	    if (timeoutID) {
	      clearTimeout(timeoutID);
	    }
	  } // Function to cancel next exec


	  function cancel() {
	    clearExistingTimeout();
	    cancelled = true;
	  } // `noTrailing` defaults to falsy.


	  if (typeof noTrailing !== 'boolean') {
	    debounceMode = callback;
	    callback = noTrailing;
	    noTrailing = undefined;
	  }
	  /*
	   * The `wrapper` function encapsulates all of the throttling / debouncing
	   * functionality and when executed will limit the rate at which `callback`
	   * is executed.
	   */


	  function wrapper() {
	    for (var _len = arguments.length, arguments_ = new Array(_len), _key = 0; _key < _len; _key++) {
	      arguments_[_key] = arguments[_key];
	    }

	    var self = this;
	    var elapsed = Date.now() - lastExec;

	    if (cancelled) {
	      return;
	    } // Execute `callback` and update the `lastExec` timestamp.


	    function exec() {
	      lastExec = Date.now();
	      callback.apply(self, arguments_);
	    }
	    /*
	     * If `debounceMode` is true (at begin) this is used to clear the flag
	     * to allow future `callback` executions.
	     */


	    function clear() {
	      timeoutID = undefined;
	    }

	    if (debounceMode && !timeoutID) {
	      /*
	       * Since `wrapper` is being called for the first time and
	       * `debounceMode` is true (at begin), execute `callback`.
	       */
	      exec();
	    }

	    clearExistingTimeout();

	    if (debounceMode === undefined && elapsed > delay) {
	      /*
	       * In throttle mode, if `delay` time has been exceeded, execute
	       * `callback`.
	       */
	      exec();
	    } else if (noTrailing !== true) {
	      /*
	       * In trailing throttle mode, since `delay` time has not been
	       * exceeded, schedule `callback` to execute `delay` ms after most
	       * recent execution.
	       *
	       * If `debounceMode` is true (at begin), schedule `clear` to execute
	       * after `delay` ms.
	       *
	       * If `debounceMode` is false (at end), schedule `callback` to
	       * execute after `delay` ms.
	       */
	      timeoutID = setTimeout(debounceMode ? clear : exec, debounceMode === undefined ? delay - elapsed : delay);
	    }
	  }

	  wrapper.cancel = cancel; // Return the wrapper function.

	  return wrapper;
	}

	/* eslint-disable no-undefined */
	/**
	 * Debounce execution of a function. Debouncing, unlike throttling,
	 * guarantees that a function is only executed a single time, either at the
	 * very beginning of a series of calls, or at the very end.
	 *
	 * @param  {number}   delay -         A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250 (or even higher) are most useful.
	 * @param  {boolean}  [atBegin] -     Optional, defaults to false. If atBegin is false or unspecified, callback will only be executed `delay` milliseconds
	 *                                  after the last debounced-function call. If atBegin is true, callback will be executed only at the first debounced-function call.
	 *                                  (After the throttled-function has not been called for `delay` milliseconds, the internal counter is reset).
	 * @param  {Function} callback -      A function to be executed after delay milliseconds. The `this` context and all arguments are passed through, as-is,
	 *                                  to `callback` when the debounced-function is executed.
	 *
	 * @returns {Function} A new, debounced function.
	 */

	function debounce (delay, atBegin, callback) {
	  return callback === undefined ? throttle(delay, atBegin, false) : throttle(delay, callback, atBegin !== false);
	}

	exports.debounce = debounce;
	exports.throttle = throttle;

	Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=index.umd.js.map


/***/ }),

/***/ 725:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_5_3_node_modules_vue_loader_dist_index_js_ref_12_0_index_vue_vue_type_style_index_0_id_9337db36_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(711);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_5_3_node_modules_vue_loader_dist_index_js_ref_12_0_index_vue_vue_type_style_index_0_id_9337db36_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_5_3_node_modules_vue_loader_dist_index_js_ref_12_0_index_vue_vue_type_style_index_0_id_9337db36_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ 726:
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

}]);